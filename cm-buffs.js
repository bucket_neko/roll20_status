/* globals state on findObjs filterObjs _ log getAttrByName */
(function(state){
    
    var buffNameAttrRegex = /repeating_buff_[^_]+_buff-name/;
    var buffEnableAttrRegex = /repeating_buff_[^_]+_buff-enable_toggle/;
    var buffEnableAttrTail = 'buff-enable_toggle';
    var buffNameAttrTail = 'buff-name';
    
    on('chat:message', function(msg){
        if (/\(cm: v\d+\.\d+\.\d+\)/.test(msg.content) === true) {
            var registerCommand = state.CampaignManager.registerCommand;

            registerCommand({
                name: 'buffs',
                usage: ['!!cm-buffs [--toggle="name"]', '!!cm-buffs [--togggle="name"] character name'],
                desc: 'Without the "--toggle" option, this command will display all of the buff that the character has and their current status (enabled or disabled). Using the "--toggle" option will toggle the current state of the buff.',
                callback: function(options, utils, msg) {
                    
                    function processForGM(options) {
                        if (options.rest.length === 0) {
                            utils.notify.GM('You must provide a character name to the --cm-buffs command.');
                            return;
                        }
                        
                        var characters = utils.findByName('character',options.rest);
                        
                        if (characters.length === 0) {
                            utils.notify.GM('No matching character found for "%s".', options.rest);
                            return;
                        }
                        
                        if (characters.length > 1) {
                            var message = '';
                            _.each(characters, function(character) {
                                var name = character.get('name');
                                message += utils.format(
                                    '[%s](!!cm-buffs %s%s)',
                                    name,
                                    (_.has(options,'toggle') === true)?
                                        '--toggle=&quot;' + options.toggle + '&quot; ':
                                        '',
                                    name
                                );
                            });
                            utils.notify(
                                'gm',
                                'Multiple Characters Found: Choose One',
                                message
                            );
                            return;
                        }
                        
                        var character = characters.pop();
                        
                        if (_.has(options,'toggle') === true) {
                            var buffs = filterObjs(function(obj){
                                if (obj.get('_type') !== 'attribute') {
                                    return false;
                                }
                                
                                if (obj.get('_characterid') !== character.id) {
                                    return false;
                                }
                                
                                if (buffNameAttrRegex.test(obj.get('name')) === false) {
                                    return false;
                                }
                                
                                if (obj.get('current').toLowerCase().indexOf(options.toggle.toLowerCase()) !== 0) {
                                    return false;
                                }
                                
                                return true;
                            });
                            
                            if (buffs.length === 0) {
                                utils.notify.GM(
                                    'No buff with the name "%s" found for character "%s".',
                                    options.toggle,
                                    character.get('current')
                                );
                                return;
                            }
                            
                            if (buffs.length > 1) {
                                var response = '';
                                _.each(buffs, function(buff){
                                    var charName = character.get('name');
                                    var buffName = buff.get('current');
                                    response += utils.format(
                                        '[%s](!!cm-buffs --toggle=&quot;%s&quot; %s)',
                                        buffName, buffName, charName
                                    );
                                });
                                
                                utils.notify(
                                    'gm', 
                                    'Multiple Buffs Found: Choose One',
                                    response
                                );
                                
                                return;
                            }
                            
                            var buff = buffs.pop();
                            
                            log(getAttrByName(character.id, buff.get('name').replace(buffNameAttrTail, buffEnableAttrTail)));
                            
                            var enableAttribute = findObjs({
                                _type: 'attribute',
                                _characterid: character.id,
                                name: buff.get('name').replace(buffNameAttrTail, buffEnableAttrTail)
                            });
                            
                            log(enableAttribute.length);
                        }
                        
                    }
                    
                    function processForPlayer(name, options) {
                        
                    }
                    
                    if (msg.isGM === true) {
                        processForGM(options);
                        return;
                    }
                    
                    processForPlayer(msg.who, options);
                }
            });
        }
    });
}(state));