/* globals state on findObjs filterObjs _ log createObj*/
(function(state){
    
    var action = [
        '/w',
        '"@{character_name}"', 
        '&{template:pf_generic}', 
        '@{toggle_accessible_flag}',
        '@{toggle_rounded_flag}',
        '{{color=@{rolltemplate_color}}}',
        '{{header_image=@{header_image-pf_generic-skill}}}',
        '{{character_name=@{character_name}}}',
        '{{character_id=@{character_id}}}',
        '{(subtitle}) {{name= Status }}',
        '{{ Current HP (Max): @{HP} (@{HP|max}) }}',
        '{{ Armor Class / Touch/ Flat footed: @{AC}/@{Touch}/@{Flat-Footed} }}',
        '{{ CMD: @{CMD} }}',
        '{{ Flat-Footed CMD: @{FF-CMD} }}',
        '{{ [Reflex Save: + @{Ref}](~@{character_id}|Ref-Save) }}',
        '{{ [Will Save: + @{Will}](~@{character_id}|Will-Save) }}',
        '{{ [Fort Save: + @{Fort}](~@{character_id}|Fort-Save) }}',
        '{{ [Initiative: + @{Init}](~@{character_id}|Roll-for-initiative)}}',
        '{{ [CMB: + @{CMB}](~@{character_id}|CMB-Check) }}',
        '{{ [All Attacks](~@{character_id}|attacks) }}',
        '{{ [All Skills](~@{character_id}|skills) }}',
        '{{ [Ability Checks](~@{character_id}|abilities) }}',
        '{{ [Defenses](~@{character_id}|defenses) }}',
        '{{ [Items](~@{character_id}|items) }}'
        ];
    
    var spellbookEntry = '{{ [@{spellclass-%d-name} Spells](~@{character_id}|spellbook-%d-roll) }}';
    var spellClassNameAttr = 'spellclass-%d-name';
    
    function characterSearch(name) {
        return filterObjs(function(obj){
            if (obj.get('_type') !== 'character') {
                return false;
            }
            var _name = obj.get('name');
            return (_name.toLowerCase().indexOf(name.toLowerCase()) === 0);
        });
    }
    
    function createAbility(name, id) {
        return createObj('ability',{
            name: name,
            _characterid: id
        });
    }
    
    on('chat:message', function(msg){
        if (/\(cm: v\d+\.\d+\.\d+\)/.test(msg.content) === true) {
            var registerCommand = state.CampaignManager.registerCommand;
            
            registerCommand({
                name: 'status',
                usage: ['!!cm-status [--setup]', '!!cm-status [--setup] character name'],
                desc: 'Creates an ability on the character which allows for the display of character information in the chat screen. This information includes: current and max hit points, armor class (regular, touch, and flat), CMD (regular and flat footed), and buttons for saves, initiatives, attacks, CMB, skill, ability checks, and spells (if the character has any). ',
                callback: function(options, utils, msg) {
                    var name;
                    
                    function populateSpellbooks(id) {
                        var book = 0;
                        var max = 10;
                        var action = [];
                        var attribute;
                        
                        for(book; book < max; book += 1) {
                            attribute = getAttrByName(id, utils.format(spellClassNameAttr,book));
                            if (attribute.length === 0) {
                                break;
                            }
                            action.push(utils.format(spellbookEntry,book,book));
                        }
                        
                        return action;
                    }
    
                    if (msg.isGM === true) {
                        if (options.rest.length === 0) {
                            utils.notify.GM('You must supply a character name to use this command.');
                            return;
                        }
                        name = options.rest;
                    } else {
                        name = msg.who;
                    }
                    
                    var characters = characterSearch(name);
                    
                    if (characters.length === 0) {
                        utils.notify.GM('There were no characters with the content "%s" in their names.', name);
                        return;
                    }
                    
                    if (characters.length > 1) {
                        var output = '';
                        _.each(characters, function(character){
                            var name = character.get('name');
                            output += utils.format(
                                '[%s](!!cm-status %s%s)',
                                name,
                                (_.has(options, 'setup') === true)?'--setup ':'',
                                name
                            );
                        });
                        utils.notify('gm','Multiple Characters: Choose One', output);
                        return;
                    }
                    
                    var character = characters.pop();
                    var ability = findObjs({
                        _type: 'ability',
                        _characterid: character.id,
                        name: 'Status'
                    });
                    var name = character.get('name');
                    
                    if (ability.length === 0) {
                        if (_.has(options, 'setup') === false) {
                            utils.notify(msg.who, 'Character Not Setup', '"%s" is not yet setup for the status command. [Setup](!!cm-status --setup %s)', name, name);
                            return;
                        }
                        createAbility('Status', character.id)
                        .set(
                            'action', 
                            action.concat(
                                populateSpellbooks(character.id)
                            ).join(' ')
                        );
                        utils.notify(msg.who, 'Character Setup', '"%s" has been setup for the status command.', name);
                        return;
                    }
                    
                    
                    
                    if (_.has(options, 'setup') === true) {
                        ability[0]
                        .set(
                            'action', 
                            action.concat(
                                populateSpellbooks(character.id)
                            ).join(' ')
                        );
                        utils.notify(msg.who, 'Character Updated', '"Status" ability for %s" has been updated.', name);
                        return;
                    }
                    
                    utils.notify(msg.who,'Status','%{%s|Status}',name);
                    
                },
                override: true
            });
        }
    });
}(state));