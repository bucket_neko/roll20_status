/**
 * Created by willi on 11/5/2016.
 */
(function () {

    var statusCmd = /^!(status(?:-setup)?)(?:\s(\w+))*/;

    var statusWhisper = '/w "@{character_name}" &{template:pf_generic} @{toggle_accessible_flag} @{toggle_rounded_flag}{{color=@{rolltemplate_color}}} {{header_image=@{header_image-pf_generic-skill}}} {{character_name=@{character_name}}} {{character_id=@{character_id}}} {(subtitle}) {{name= Status }} {{ Current HP (Max): @{HP} (@{HP|max}) }} {{ Armor Class / Touch/ Flat footed: @{AC}/@{Touch}/@{Flat-Footed} }} {{ CMD: @{CMD} }} {{ Flat-Footed CMD: @{FF-CMD} }} {{ [Reflex Save: + @{Ref}](~@{character_id}|Ref-Save) }} {{ [Will Save: + @{Will}](~@{character_id}|Will-Save) }} {{ [Fort Save: + @{Fort}](~@{character_id}|Fort-Save) }} {{ [Initiative: + @{Init}](~@{character_id}|Roll-for-Initiative)}} {{ [CMB: + @{CMB}](~@{character_id}|CMB-Check) }} {{ [All Attacks](~@{character_id}|attacks_buttons_macro) }} {{ [All Skills](~@{character_id}|skills_buttons_macro) }} {{ [Ability Checks](~@{character_id}|ability_checks) }}';
    var spellbookEntry = '{{ [@{spellclass-$d-name} Spells](~@{character_id}|spellbook-$d-roll) }}';
    var errors = {
        style: function(msg) { return '<div style="background-color: rgba(255,0,0,.5);color: white">' + msg + '</div>' },
        mustProvideACharacterName: function(who) { return '/w ' + who + ' ' + errors.style('!status-setup error: the GM must provide the character\'s first name.'); },
        noSuchCharacter: function(who, name) { return '/w ' + who + ' ' + errors.style('No such character "' + name + '".') }
    };

    var statusChat = '%{name|Status}';

    /**
     * Used to validate the presense of a spellbook. It attempts to get the attribute 'spellclass-$0-name' from the
     * character object references by the 'id' parameter, where $0 is a number from 0-9. If the attribute exists,
     * the function returns 'true'.
     *
     * @param book {Number} A number from 0 to 9.
     * @param id {String} The id for the character object to inspect.
     * @returns {boolean}
     */
    function spellbookCheck(book, id) {
        return getAttrByName(id,['spellclass',book,'name'].join('-')).length > 0;
    }

    /**
     * A helper function to send whisper messages to the chat window.
     *
     * @param who {String} The character name or id to whisper to.
     * @param msg {String} The message to send.
     */
    function notify(who, msg) {
        sendChat('Status Processor', ['/w',who,msg].join(' '));
    }
    
    /**
     * Retrives the character object for the character name either passed to the command or
     * retrieved from the msg object.
     * 
     * @param msg {{}} The message object passed from the chat interceptor.
     * @param msg.isGM {boolean} Indicates if the player requesting the information is the GM.
     * @param msg.validCmd {[]} The resulting array from the command Regular Expression execution.
     * @param msg.validCmd[2] {string} The character name requested, if any.
     * @param msg.who {string} The name of the character passed from the chat interceptor.
     * 
     * @returns {*}
     */
    function getCharacter(msg) {
        var character;

        if (msg.isGM === true) {
            if (msg.validCmd.length < 3) {
                sendChat(
                    'Status Setup',
                    errors.mustProvideACharacterName(msg.who)
                );
                return;
            }
            character = filterObjs( function(obj) {
                return (
                    obj.attributes._type === 'character'  &&
                    obj.attributes.name.indexOf(msg.validCmd[2]) === 0
                );
            });
        } else {
            character = findObjs({
                _type: 'character',
                name: msg.who
            });
        }

        if (character.length === 0) {
            sendChat(
                'Status Processor',
                errors.noSuchCharacter(
                    msg.who,
                    (msg.isGM === true)?msg.validCmd[2]:msg.who
                )
            );
            return;
        }

        return character[0];
    }

    /**
     * A helper function to retrieve a Roll 20 ability object from a character.
     * By default if the ability cannot be found, it will be created on the character.
     * This functionality can be changed using the options parameter. Nornally
     * the options.onSuccess function is used to act on the ability.
     * 
     * @param id {string} The identification string of the character to use.
     * @param name {string} The name of the ability to retrieve. This is case-sensitive.
     * @param options {{}} An object detailing options to use.
     * @param [options.createOnFail=true] {boolean} A flag to indicate if the ability
     * should be created if it isn't found. Be default this is true.
     * @param [options.onSuccess] {function} The function to call when the ability
     * is found, or after it is created. This function will be passed the ability
     * object and the name of the ability. By default this is an empty function.
     * @param [options.preCreate] {function} The function to call prior to creating
     * the ability. Normally used for logging purposes or to pass information to
     * the player. By default this is an empty function.
     * @param [options.postCreate] {function} The function to call after creating
     * the ability but before calling the onSuccess function. By default this is
     * an empty function.
     * @param [options.onFail] {function} The function called if the ability isn't
     * found. This is only called if the createOnFail option is set to false.
     */
    function getAbilityByName(id, name, options) {
        var _ability = findObjs({
            _characterid: id,
            _type: 'ability',
            name: name
        });
        options = _.extendOwn({
            onSuccess: _.noop,
            onFail: _.noop,
            preCreate: _.noop,
            postCreate: _.noop,
            createOnFail: true
        },options);

        if (_ability.length > 0) {
            options.onSuccess(_ability[0], name);
            return;
        }

        if (options.createOnFail === true) {
            options.preCreate(name);
            _ability = createObj(
                'ability',
                {
                    name: name,
                    characterid: id,
                    istokenaction: true
                }
            );
            options.postCreate(name);
            options.onSuccess(_ability, name);
            return;
        }

        options.onFail(name);
    }

    /**
     * Updates the action property of the status ability. It uses the statusWhisper
     * variable as the basis for the action content and then iterates through the
     * spellbook names. So long as the name attribute value is populated, the
     * function will create a button for the spellbook in the action. The first
     * blank name it finds causes the iteration to halt. Also, if the iteration
     * passes the final spell book, the iteration also stops.
     * 
     * @param id {string} The identification string for the character to update.
     * @param ability {string} The ability object to use for the update.
     * @param name {string} The name of the character being updated.
     * @param who {string} The name of the player that initiated the call.
     */
    function updateStatusAbility(id, ability, name, who) {
        notify(
            who,
            'Updating "' + ability.get('name') + '" ability for ' + name + '...'
        );

        log(ability);
        var book = 0;
        var lastBook = 9;
        var action = statusWhisper;

        while(spellbookCheck(book,id) === true || book > lastBook) {
            action += ' ' + spellbookEntry.replace(/\$d/g, book);
            book += 1;
        }

        ability.set('action',action);
    }
    
    /**
     * Creates and / or updates the Status ability for the character.
     * 
     * @param msg {{}} The object passed from the chat interceptor.
     */
    function setup(msg) {
        var character = getCharacter(msg);
        if (character === undefined) {
            return;
        }
        var name = character.get('name');
        getAbilityByName(
            character.id,
            'Status',
            {
                onSuccess: function(ability) {
                    updateStatusAbility(character.id, ability, name, msg.who);
                },
                preCreate: function(_name) {
                    notify(msg.who, 'Unable to find ability "' + _name + '" for character "' + name + '". Creating...');
                },
                postCreate: function(_name) {
                    notify(msg.who, 'Ability "' + _name + '" created for character "' + name + '".');
                }
            }
        );
    }
    
    /**
     * Displays the contents of the Status ability for the character.
     * 
     * @param msg {{}} The object passed from the chat interceptor.
     */
    function status(msg) {
        var character = getCharacter(msg);
        if (character === undefined) {
            return;
        }

        getAbilityByName(
            character.id,
            'Status',
            {
                onSuccess: function(){
                    notify(
                        msg.who,
                        [statusChat.replace('name', character.get('name'))].join(' ')
                    );
                },
                onFail: function(name) {
                    notify(
                        msg.who,
                        character.get('name') + ' does not have the ability "' + name + '". Please run command "!status-setup".'
                    )
                },
                createOnFail: false
            }
        );
    }

    on('chat:message', function (msg) {
        msg.isGM = playerIsGM(msg.playerid);
        msg.isAPI = (msg.type === 'api');
        msg.validCmd = statusCmd.exec(msg.content);

        if (msg.isAPI !== true) {
            return;
        }

        if (msg.validCmd === null) {
            return;
        }

        if (msg.validCmd[1] === 'status-setup') {
            setup(msg);
        }

        if (msg.validCmd[1] === 'status') {
            status(msg);
        }

    });

}());