/**
 * Created by nekokun on 6/10/2016.
 */
 /* globals _ log sendChat state on playerIsGM filterObjs findObjs */
 
(function (state) {
    
    _.delay(function(){
        var currentVersion = 'v0.5.1';
        var baseNotifyFormat = '<div style="border: 2px solid black;cursor: default;"><div style="background-color: rgba(0, 0, 0, 1); color: rgba(255,255,255,1); border-bottom: 1px solid black; text-align: center;">%s</div><div style="min-height:1em;padding: 2px;">%s</div></div>';
        var resetPassword='here be dragons';
        
        
        _.mixin({
            hasAll: function(object) {
                var properties = _.tail(_.toArray(arguments));
                var propLength = properties.length;
                
                if (propLength === 0) {
                    return false;
                }
                
                for(var index = 0; index < propLength; index += 1) {
                    var property = properties[index];
                    if (_.has(object, property) === false) {
                        return false;
                    }
                }
                
                return true;
            }
        });
        
        // ********************************************************************
        // Utility Functions
        // ********************************************************************
    
        /**
         * Ensures that the passed item is of the same type as the default_value.
         * This function uses the Object.prototype.toString call to determine if
         * the two objects are of the same type. If the two types are the same then
         * the item is returned. Otherwise the default_value is returned.
         * 
         * @param item {*} The item to evaluate.
         * @param default_value {*} The value to return if the item type does not
         * match.
         * @returns {*}
         */
        function guarantee(item, default_value) {
            var itemType = Object.prototype.toString.call(item);
            var defaultValueType = Object.prototype.toString.call(default_value);
            
            if (itemType === defaultValueType) {
                return item;
            }
    
            return default_value;
        }
    
        
        /***
         * Replaces placeholders located in the String object with
         * the appropriate item from the arguments list. This works
         * like the Java String.format method though only %s and %d
         * are supported.
         *
         * If a match for a placeholder cannot be found, the placeholder
         * is cleared out, to prevent an endless loop from occurring.
         *
         * @param string {String} The String to parse through
         * @returns {string}
         */
        function format(string) {
            var args = _.toArray(arguments).slice(1);
            var placeholderRegex = /%(\w)/g;
            var clone = string;
            var matcher;
            var replacement = '';
            var rIndex;
    
            if (args.length === 0) {
                return clone;
            }
            matcher = placeholderRegex.exec(string);
            
            while (_.isArray(matcher)) {
                var placeholder = matcher.shift();
                var type = matcher.shift();
                switch (type.toLowerCase()) {
                    case 's':
                        rIndex = _.findIndex(args, _.isString);
                        if (rIndex !== -1) {
                            replacement = args.splice(rIndex, 1);
                        }
                        break;
                    case 'd':
                        rIndex = _.findIndex(args, _.isNumber);
                        if (rIndex !== -1) {
                            replacement = args.splice(rIndex, 1);
                        }
                        break;
                    default:
                    // do nothing
                }
                clone = clone.replace(placeholder, replacement);
                matcher = placeholderRegex.exec(string);
            }
            return clone;
        }
    
        /**
         * Maps an array of values to a list of property names and returns an object
         * configured with the names.
         * 
         * This function accepts an array (args) and number corresponding to the
         * length of the array to map. If this isn't provided, or isn't a number,
         * it will default to the length of the provided array. This function also
         * expects at least one more array to be passed which contains a list of
         * property names. The names should correspond to the corresponding index
         * locations of args.
         * 
         * Multiple mapping arrays may be passed, to indicate different configurations
         * for the object. The length of the args array will be used to determine
         * which of the secondary arrays will be used to create the object. If a
         * corresponding array isn't located, the first array will be used.
         * 
         * If a mapping array is found, the function will iterate through that
         * array and create properties for each of the values in it, with property
         * values equal to the value at that index in the args array. Before the
         * object is returned, a read-only property named '__matches' is create
         * to inform the calling function of how many properties were matched.
         * 
         * @param args {[]} The source array.
         * @param to {number} The upper index (excluded) from the source array to use.
         * @param [mappings*] {[]} The list of arrays to act as mapping sources.
         */
        function mapArguments(args, to) {
            var _mapping = guarantee(_.tail(_.toArray(arguments), 2), []);
            var _args = guarantee(args, []);
            var _to = guarantee(to, _args.length);
            
            if (_args.length > _to) {
                _args = _.first(_args, _to);
            }
            
            var argLength = _args.length;
            var map = {};
            var matches = 0;
            var _useMapping = _.find(_mapping, function(list){
                return (
                    _.isArray(list) &&
                    list.length === argLength
                );
            });
            
            if (_useMapping === undefined) {
                _useMapping = _mapping[0];
            }
            
            _.each(_useMapping, function(prop, index){
                if (index < argLength) {
                    map[prop] = _args[index];
                    matches += 1;
                }
            });
            
            Object.defineProperty(
                map, 
                '__matches', 
                {
                    value: matches
                }
            );
            
            return map;
        }
    
        /**
         * A wrapper for the log() function that prepends the message with the
         * date / time in UTC as well as a severity and category, much like some
         * Unix loggers. The functin will also apply formatting to the message if
         * enough arguments are passed.
         * 
         * The function has three signatures:
         * logU(msg) - it will automatically apply the default severity (debug) and
         * category (general) to the message.
         * logU(category, msg) - it will automatically apply the default category.
         * logU(severity, category, msg)
         * 
         * @param [severity=debug] {string} The severity to prepend to the message.
         * @param [category=general] {string} The category to prepend to the message.
         * @param msg {string} The base message to pass to the log function.
         * @param [formatVariables*] {number|string} The variables to pass to format the message.
         */
        function logU(severity, category, message) {
            if (message === undefined) {
                log('No message passed to logU()');
                return;
            }
            
            var _severity = guarantee(severity, 'debug');
            var _category = guarantee(category, 'general');
            var _message = format(message, _.tail(_.toArray(arguments), 3));
            var now = new Date();
    
            log(format(
                '%s - %s [%s]: %s ',
                now.toString(),
                _severity,
                _category,
                _message
            ));
        }
        
        /**
         * A shortcut method which automatically adds the severity 'debug' to the
         * logU() function.
         * 
         * @param msg {string} The message to pass to the logU function.
         * @param [formatVariables*] {number|string} The variables to pass to format the message.
         */
        logU.debug = function(msg) {
            logU.apply(logU,[undefined, undefined].concat(_.toArray(arguments)));
        };
        
        /**
         * A shortcut method which automatically adds the severity 'warn' to the
         * logU() function.
         * 
         * @param msg {string} The message to pass to the logU function.
         * @param [formatVariables*] {number|string} The variables to pass to format the message.
         */
        logU.warn = function(msg) {
            logU.apply(logU, ['warn',undefined].concat(_.toArray(arguments)));
        }
        
        /**
         * Dispalys a notification in the chat window. The notification is a whisper
         * sent to the 'who'. The whisper is formatted with the baseNotifyFormat
         * variable value using the 'titile' and 'message'. The 'message' is formatted
         * prior to adding it to the baseNotifyFormat. The function also logs the
         * message with the logU() function.
         * 
         * @param who {string} The player to send the notification to.
         * @param title {string} The title to set the notification.
         * @param message {string} The message to display in the notification. This message may be formatted using the format() function syntax.
         * @param [formatArgs*] {(string|number)} Values to use in the formatting of the message.
         */
        function notify(who, title, message) {
            var args = _.toArray(arguments);
    
            var msg = format.apply(
                format,
                [message].concat(_.tail(_.toArray(arguments),3))
            );
            
            var notification = format(
                '/w %s %s',
                who, format(
                    baseNotifyFormat,
                    guarantee(title,'Notification'),
                    msg
                )
            );
    
            logU.debug(msg);
            sendChat('Campaign Manager', notification);
        }
    
        /**
         * A shortcut function used to send a notification to the GM.
         */
        notify.GM = function (message) {
            notify.apply(notify, ['gm', 'Notification'].concat(_.toArray(arguments)));
        };
    
        notify.gm = notify.GM;
    
        /**
         * Displays a notification to everyone using the '/direct' syntax.
         */
        notify.all = function(title, message) {
            var msg = format.apply(format, _.tail(_.toArray(arguments), 1));
            logU.debug(msg);
            sendChat(
                'Campaign Manager', 
                format('/direct %s',
                    format(
                        baseNotifyFormat,
                        guarantee(title, 'Notification'),
                        msg
                    )
                )
            );
        };
        
        /**
         * Attempts to register a command to the manager. The function expects
         * an object with certain properties to facilitate the registration of the
         * command. If the command is already registered with the manager, the
         * request will fail.
         * 
         * @param specs {{}} The object containing the information about the command.
         * @param specs.name {string} The name of the command.
         * @param specs.usage {string | array(string) } The usage for the command. This should follow the typical POSIX (Unix) usage syntax. If an array is passed, it's presumed that the first element is for players while the second is for GMs.
         * @param specs.desc {string} The description of the command.
         * @param [specs.gmOnly=false] {boolean} A flag to indicate if the command can only be executed by the GM.
         * @param specs.callback {function} The function to execute when the command is intercepted. The function will be passed several arguments: an options object, containing the options passed to the command, the managers utils object, the msg object passed from the chat interceptor. The msg object will have a isGM property added which will indicate if the player executing the message is the GM. Any message which indicates that it is for a GM only will not be executed if the player is not the GM.
         */
        function registerCommand(specs) {
            specs.name = guarantee(specs.name, '');
            specs.desc = guarantee(specs.desc, '');
            specs.gmOnly = guarantee(specs.gmOnly,false);
            specs.override = guarantee(specs.override,false);
            
            if ((_.isArray(specs.usage) || _.isString(specs.usage)) === false) {
                specs.usage = '';
            }
            
            if (specs.name === '') {
                logU.warn('No command name passed to registerCommand().')
                return;
            }
            
            if (_.isFunction(specs.callback) === false) {
                logU.warn('No callback passed to registerCommand().');
                return;
            }
            
            if (_.has(state.CampaignManager.commands, specs.name) === true) {
                if (specs.override === false) {
                    notify('gm','Command Alredy Registered', '"%s" is already registered with the campaign manager.', specs.name);
                    return;
                }
            }
            state.CampaignManager.commands[specs.name] = specs;
            notify('gm', 'Command Registered', '"%s" has been successfully registered with the campaign manager', specs.name);
        }
        
        /***
         * Inspects the commands object for the command specified by the name
         * argument. Returns true if the name exists, false otherwise.
         * 
         * @param name {string} The name of the command to search for; this is
         * case sensitive.
         * @returns {boolean}
         */
        function hasCommand(name) {
            return _.has(state.CampaignManager.commands, name);
        }
        
        /**
         * Wraps around the Roll 20 filterObj function to provide the ability
         * to find objects by their name. This search is case insensitive and
         * matches names from the beginning of the string. Since it searches
         * the entire list of objects, it can be slow. Use with caution.
         * 
         * @param type {string} The type of Roll 20 object to search for.
         * @param name {string} The name to filter against.
         * @returns {[]}
         */
        function findByName(type, name) {
            var _type = guarantee(type,'');
            var _name = guarantee(name, '');

            if (_name.length === 0 || _type.length === 0) {
                return [];
            }
            
            return filterObjs(function(obj){
                
                if (obj.get('_type') !== _type) {
                    return false;
                }
                
                return (
                    obj.get('name').toLowerCase().indexOf(_name.toLowerCase()) === 0
                );
            });
        }
        
        /***
         * Returns an array of character objects which begin with the passed
         * name. (Basically a wrapper for the findByName(type, name) function
         * with 'type' being 'character'.)
         * 
         * @param name {string} The name to filter against.
         * @returns {[]}
         */
        findByName.character = function(name) {
            return findByName('character', name);
        }
        
        /***
         * Returns an array of token objects which begin with the passed name.
         * (Basically a wrapper for the findByName(type, name) function with
         * 'type' being 'graphic' further filtered on '_subtype' being 'token'.
         * 
         * @param name {string} The name to filter against.
         * @returns {[]}
         */
        findByName.token = function(name) {
            return _.filter(findByName('graphic', name), function(obj){
                return obj.get('_subtype') === 'token';
            });
        }
        
        /**
         * Attempts to store the passed value in the manager's storage namespace.
         * If the name is not a valid string or the value is undefined, or null,
         * the function will do nothing.
         * 
         * @param name {string} The name to use when storing the value.
         * @param name {*} The value to store. Note: items which aren't serialized by the JSON.stringify() method will be lost at the next sandbox cycle.
         */
        function store(name, value) {
            var _name = guarantee(name,'');
            
            if (_name.length === 0) {
                logU.warn('store() requires a valid string for a "name" parameter.');
                return;
            }
            
            if (value === undefined || value === null) {
                logU.warn('You cannot store undefined, or null, values with store().');
                return;
            }
            
            if (_.isFunction(value) === true || _.isRegExp(value) === true) {
                logU.warn('Values which aren\'t serialized by the JSON.stringify() method will be lost at the next sandbox cycle.');
            }
            
            state.CampaignManager.storage[name] = value;
        }
        
        /**
         * Recovers a value stored in the managers storage property. If the
         * value was of a type that is not serialized by the JSON.stringify()
         * method, it will be lost when the sandbox is reset.
         * 
         * @param name {string} The name of the storage space to retrieve.
         * @returns {*}
         */
        function retrieve(name) {
            var _name = guarantee(name,'');
            
            if (_name.length === 0) {
                logU.warn('retrieve() requires a valid string for a "name" parameter.');
                return;
            }
            
            if (_.has(state.CampaignManager.storage, _name) === false) {
                logU.warn('"%s" is not currently stored', _name);
                return;
            }
            
            return state.CampaignManager.storage[_name];
        }

        /**
         * Evaluates if the stored version for the Campaign Manager is the
         * current version indicated from the main variable.
         * 
         * @param cm {{}} The Campaign Manager object stored in the state object.
         * @param cm.configuration {{}} The Campaign Manager configuration property.
         * @param cm.configuration.version {string} The latest version of the Campaign Manager stored.
         */
        function versionHasUpdated(cm) {
            return (cm.version !== currentVersion);
        }
        
        /**
         * Returns the default configuration object  with properties of version
         * and templates.
         */
        function configure() {
            return {
            	version: currentVersion ,
            	templates: {
            		notify: '<div style="border: 2px solid black;"><div style="background-color: rgba(255, 255, 255, 1); color: rgb(0,0,0); border-bottom: 1px solid black; text-align: center;">%s</div><div style="min-height:1em">%s</div></div>'
            	}
            };
        }
        
        /**
         * Does the initial configuration of the CampaignManager namespace.
         * If the CampaignManager namespace doesn't exist in the state object,
         * or if the current version (stored in the currentVersion variable
         * at the top) of the Campaign Manager is different from the version
         * stored in the namespace, the return from the configure() method
         * is passed back to the caller after being merged with the volitiles
         * object.
         * 
         * Otherwise, the current state of the namespace is merged with the
         * volitiles object and this merged object is passed back to the caller.
         * 
         * @param state {{}} The Roll 20 global state object, set as a parameter
         * to keep the function as pure as possible.
         */
        function doConfiguration(state) {
            var volitiles = {
            	commands: {},
            	syntax: {
            		command: /^!!cm-(\w[-\w]+)\b/i,
            		options: {
            			singleQuotes: /--(\w+)='(.*)'/,
            			doubleQuotes: /--(\w+)="(.*)"/,
            			noQuotes: /--(\w+)(?:=([^\s]+))?/
            		}
            	},
            	utils: {
            		logU: logU,
            		notify: notify,
            		format: format,
            		store: store,
            		retrieve: retrieve,
            		bank: store,
            		withdrawl: retrieve,
            		findByName: findByName,
            		hasCommand: hasCommand,
            		hasModule: hasCommand
            	},
            	registerCommand: registerCommand
        	};

            if (
                _.has(state, 'CampaignManager') === false || 
                versionHasUpdated(state.CampaignManager) === true
            ) {
                return _.extendOwn(configure(), volitiles);
            } 
            return _.extendOwn(state.CampaignManager,volitiles);
        }
        
        /**
         * Registers the help command with the manager.
         */
        function registerHelpCommand() {
            registerCommand({
                name: 'help',
                usage: '!!cm-help',
                desc: 'Displays all of the commands that you may use.',
                callback: function(options, utils, msg) {
                    var commands = state.CampaignManager.commands;
                    var cells = '<div style="border: 1px solid black;margin-bottom: 5px;"><div style="border-bottom: 1px solid black;text-align: center;background-color:green;color:yellow;">%s [ %s ]</div><div style="text-indent: 10px;">%s</div></div>';
                    var output = '';
                    
                    _.mapObject(commands, function(command){
                        if (command.gmOnly === true) {
                            if (msg.isGM === false) {
                                return;
                            }
                        }
                        
                        if (msg.isGM === true) {
                            output += format(cells,command.name,(_.isArray(command.usage)===true)?command.usage[1]:command.usage,command.desc);
                            return;
                        }

                        output += format(cells,command.name,(_.isArray(command.usage)===true)?command.usage[0]:command.usage,command.desc);
                    });
                    utils.notify(msg.who,'Campaign Manager Commands',output);
                }
            });
        }
        
        /**
         * Registers the reset command with the manager.
         */
        function registerResetCommand() {
            registerCommand({
                name: 'reset',
                usage: '!!cm-reset --confirm="<password>"',
                desc: 'Performs a hard reset of the Campaign Manager namespace, reloading all of the commands and emptying the storage. Do not do this unless you\'re absolutely certain about it.',
                gmOnly: true,
                callback: function(options, utils) {
                    if (options.confirm !== resetPassword) {
                        utils.notify.GM('You must provide the reset password in order to execute this command.');
                        return;
                    }
                    var currentCommands = _.extendOwn({},state.CampaignManager.commands);
                    state.CampaignManager = configure();
                    state.CampaignManager.commands = _.extendOwn(state.CampaignManager.commands, currentCommands);
                    utils.notify.GM('Reset of the Campaign Manager is complete.');
                }
                
            });
        }
        
        /**
         * Iterates through the command line and retrieves the options passed
         * in it. An option is a sequence of letters, numbers, or underscores
         * preceeded by a double-dash (--). It may be followed by an equal-sign
         * (=) and then a value: a sequence of characters without a
         * space, or a sequence characters surrounded by double-quotes.
         * 
         * When an option is found using the passed regex object,
         * the function will store the option name and value, if any, in a 
         * plain object. If there is no value given then the stored value will
         * be the boolean true. The found option is then removed from the 
         * passed command_line variable.
         * 
         * Once all of the options have been retrieved from the command_line
         * the function will return an array with the first element being the
         * populated object and the second being the modified command_line.
         * 
         * @param regex {RegExp} The regular expression object to use for
         * parsing the command_line.
         * @param command_line {string} The content from the chat message sent.
         * @returns {[]}
         */
        function getOptions(regex, command_line) {
            var options = {};
            var option = regex.exec(command_line);
            
            while( _.isNull(option) === false) {
                if (option[0].indexOf('=') > -1){
                    options[option[1]] = option[2];
                } else {
                    options[option[1]] = true;
                }
                command_line = command_line.replace(option[0],'');
                option = regex.exec(command_line);
            }
            
            return [options, command_line];
        }
        
        /**
         * Sets up the chat:message interceptor function. Any chat message that
         * comes in must meet the following criteria in order to be processed:
         * - the leading command must match the Campaign Manager command filter
         * (!!cm-).
         * - the command must have been registered with the manager.
         * - if the command is listed as only for GMs, then executor must be
         * the GM.
         * If all of these conditions are met, then the function will process
         * the message content for options and execute the command's callback
         * method, passing the options, util functions, and msg object as
         * the parameters.
         */
        function startInterceptor() {
            var commandRegex = state.CampaignManager.syntax.command;
            var optionsRegex = state.CampaignManager.syntax.options;
            var commands = state.CampaignManager.commands;
            var utils = state.CampaignManager.utils;
            
            on('chat:message', function(msg) {
                var _commandTest = commandRegex.exec(msg.content);
                
                if (_.isNull(_commandTest) === true) {
                    return;
                }
                
                var _command = _commandTest[1]; 
                
                if (_.has(commands, _command) === false) {
                    notify(msg.who,'No such command','"%s" command is not registered with the manager.', _command);
                    return;
                }
                
                
                msg.isGM = playerIsGM(msg.playerid);
                _command = commands[_command];
                
                if (_command.gmOnly === true) {
                    if (msg.isGM === false) {
                        return;
                    }
                }
                
                var options = {};
                var content = msg.content.replace(_commandTest[0],'');
                var response;
                
                _.each(
                    [optionsRegex.doubleQuotes, optionsRegex.noQuotes],
                    function(regex) {
                        response = getOptions(regex, content);
                        options = _.extendOwn(options, response[0]);
                        content = response[1].trim();
                    }
                );
                
                options.rest = content.trim();

                
                _command.callback(options, utils, msg);
            });
        }
        
        state.CampaignManager = doConfiguration(state);
        registerHelpCommand();
        registerResetCommand();
        
        startInterceptor();
        
        notify.gm('-=> Campaign Manager <=- Ready. (cm: %s)', state.CampaignManager.version);
        notify.all('Notification', 'This campaign uses the Campaign Manager, "ver: %s". Click <a href="!!cm-help">Help</a> to see the syntax.', state.CampaignManager.version);
    }, 2000);
}(state));