/***
 * 
 * !!cm-add-effect (--name="Unknown" --duration="1" --increment="-1"|--round|--set-macro)
 *
 * Adds an effect to the end of the turn tracker. The name, duration, and 
 * increment values are optional, but if they are omitted the values presented
 * in the usage will be used instead. If the "round" option is used a round
 * counter will be added to the end of the turn order.
 * 
 * The "set-macro" option will create a macro named "Add-Effect" which will
 * prompt you for the values of duration, name, and increment.
 * 
 */

/* globals state on findObjs filterObjs _ log */
(function(state){
    
    var defaultMacros = [
        {
            name: 'Add-Effect',
            action: '!!cm-add-effect --name=?{Name|Unknown} --duration=?{Duration|1} --increment=?{Increment|-1}'
        },
        {
            name: 'Add-Round-Counter',
            action: '!!cm-add-effect --round'
        }
    ];
    
    on('chat:message', function(msg){
        if (/\(cm: v\d+\.\d+\.\d+\)/.test(msg.content) === true) {
            var registerCommand = state.CampaignManager.registerCommand;

            registerCommand({
                name: 'add-effect',
                usage: '!!cm-add-effect (--name="Unknown" --duration="1" --increment="-1"|--round|--addmacros)',
                desc: 'Adds an effect to the end of the turn tracker. The name, duration, and increment values are optional, but if they are omitted the values presented in the usage will be used instead.<br><br>If the "round" option is used a round counter will be added to the end of the turn order.<br><br>The "addmacros" option will create a series of helper macros for you.',
                callback: function(options, utils, msg) {
                    var characterName;
                    var turnOrder = utils.guarantee(JSON.parse(Campaign().get('turnorder')),[]);
                    var effectName = utils.guarantee(options.name,'Unknown');
                    var effectDuration = parseInt(utils.guarantee(options.duration,'1'));
                    var effectIncrement = parseInt(utils.guarantee(options.increment,'-1'));
                    
                    if (Campaign().get('initiativepage') === false) {
                        utils.notify.GM('The turn order tracker must be active to use this command.');
                        return;
                    }
                    
                    var effect = { id: '-1' };
                    
                    log(options);
                    
                    if (_.has(options, 'addmacros') === true) {
                        var macros = findObjs({
                            _type: 'macro',
                            _playerid: msg.playerid
                        });
                        
                        var created = 0;
                        
                        _.each(defaultMacros, function(_macro){
                            var found = utils.guarantee(
                                findObjs({
                                    _type: 'macro',
                                    _playerid: msg.playerid,
                                    name: _macro.name
                                }),
                                []
                            );
                            
                            if (found.length === 0) {
                                createObj('macro',_.extendOwn(_macro,{_playerid: msg.playerid}));
                                created += 1;
                            }
                        });
                        
                        if (created > 0) {
                            utils.notify.GM('Some or all of the default macros have been created. You may wish to check the "In Bar" option for each.');
                        } else {
                            utils.notify.GM('The default macros have already been created.');
                        }
                        
                        return;
                    }
                    
                    if (_.has(options,'round') === true) {
                        
                        if (_.findWhere(turnOrder,{custom: 'Round'}) !== undefined) {
                            utils.notify.GM('A round counter is already present in the turn tracker.');
                            return;
                        }
                        
                        turnOrder.push(_.extendOwn(effect, {
                            custom: 'Round',
                            pr: 0,
                            formula: +1
                        }));
                    } else {
                        turnOrder.splice(1,0,_.extendOwn(effect, {
                            custom: utils.format("Effect: %s",effectName),
                            pr: effectDuration,
                            formula: effectIncrement
                        }));
                    }
                    
                    log(turnOrder);
                    Campaign().set('turnorder',JSON.stringify(turnOrder));
                },
                override: true,
                gmOnly: true
            });
            
            on('change:campaign:turnorder', function(){
                var turnOrder = JSON.parse(Campaign().get('turnorder'));
                
                if (_.isArray(turnOrder) === false) {
                    return;
                }
                
                if (turnOrder.length === 0) {
                    return;
                }
                
                var recent = _.last(turnOrder);
                
                if (recent.custom.indexOf('Effect:') !== 0) {
                    return;
                }
                
                if (parseInt(recent.pr) < 1) {
                    turnOrder.pop();
                    sendChat('Event Tracker','/desc <div style="background-color:red; color: white;font-variant:small-caps;">"' + recent.custom + '" has ended.</div>' );
                }
                
                Campaign().set('turnorder', JSON.stringify(turnOrder));
            });
        }
    });
}(state));